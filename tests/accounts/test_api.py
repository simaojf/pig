import base64
import uuid

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from accounts.models import User
from .test_models import UserFactory


def get_basic_auth_header(username, password):
    return 'Basic %s' % base64.b64encode(
        ('%s:%s' % (username, password)).encode('ascii')).decode()


class UserRegisterTests(APITestCase):

    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=False,
            email='spider@pigworld.com'
        )
        self.user.set_password('acorns')
        self.user.save()

    def test_register_missing_data_fail(self):
        """Test cases of failure, due to missing data on request."""
        url = reverse('accounts:register')

        # Missing required data (email).
        data = {
            'password': 'acorns',
            'confirm_password': 'acorns'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'email': ['This field is required.']})

        # Missing required data (password).
        data = {
            'email': 'babe@pigworld.com',
            'confirm_password': 'acorns'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'password': ['This field is required.']})

        # Missing required data (confirm_password).
        data = {
            'email': 'babe@pigworld.com',
            'password': 'acorns'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'confirm_password': ['This field is required.']})

    def test_register_fail(self):
        """Test cases of failure."""
        url = reverse('accounts:register')

        # Method not allowed.
        response = self.client.get(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Passwords don't match.
        data = {
            'first_name': 'Babe',
            'last_name': 'The pig',
            'email': 'babe@pigworld.com',
            'password': 'acorns',
            'confirm_password': 'bacon'  # not the same
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'non_field_errors': ["The passwords don't match."]})

        # Email already registered.
        data = {
            'first_name': 'Babe',
            'last_name': 'The pig',
            'email': 'spider@pigworld.com',  # default user on setup
            'password': 'acorns',
            'confirm_password': 'acorns'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {'email': ['user with this email address already exists.']}
        )

        # Email not allowed.
        data = {
            'first_name': 'Babe',
            'last_name': 'The pig',
            'email': 'spider@example.com',  # bad email
            'password': 'acorns',
            'confirm_password': 'acorns'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'email': ['Invalid email address.']})

    def test_register_success(self):
        """Test successful cases of user registration."""
        url = reverse('accounts:register')
        data = {
            'first_name': 'Babe',
            'last_name': 'The pig',
            'email': 'babe@pigworld.com',
            'password': 'acorns',
            'confirm_password': 'acorns'
        }
        # Check response.
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check database.
        user = User.objects.get(
            first_name=data['first_name'], last_name=data['last_name'],
            email=data['email']
        )
        self.assertFalse(user.is_active)  # check is not active


class UserLoginTests(APITestCase):

    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=True,
            email='spider@pigworld.com'
        )
        self.user.set_password('acorns')
        self.user.save()

    def test_account_login_unsuccessful(self):
        """Test cases of failure."""
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'bacon')
        )
        response = self.client.post(reverse('accounts:login'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_account_login_successful_and_perform_actions(self):
        """Test successful login."""
        url = reverse('accounts:login')
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'acorns')
        )
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)


class UserConfirmTests(APITestCase):

    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=False,
            email='spider@pigworld.com'
        )
        self.user.set_password('acorns')
        self.user.save()

    def test_confirm_fail(self):
        """Test cases of failure."""
        # Test with random uuid on url.
        url = reverse('accounts:email-confirmation', args=(str(uuid.uuid4()),))

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_register_success(self):
        """Test successful cases of user registration."""
        url = reverse('accounts:email-confirmation',
                      args=(self.user.activation_key,))

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
