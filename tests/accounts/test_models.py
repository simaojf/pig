import factory

from django.test import TestCase

from accounts.models import User


class UserFactory(factory.DjangoModelFactory):

    class Meta:
        model = User
        django_get_or_create = ('email',)


class UserModelsTests(TestCase):
    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=False,
            email='spider@pigworld.com'
        )

    def test_string(self):
        """Check if str is correct."""
        self.assertEqual(str(self.user), 'spider@pigworld.com')

    def test_get_full_name(self):
        """Test full name."""
        self.assertEqual(self.user.get_full_name(), 'Spider Pig')

    def test_get_short_name(self):
        """Test short name."""
        self.assertEqual(self.user.get_short_name(), 'Spider')
