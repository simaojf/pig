import base64

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from tests.accounts.test_models import UserFactory
from tests.contents.test_models import PigLatinContentFactory


def get_basic_auth_header(username, password):
    return 'Basic %s' % base64.b64encode(
        ('%s:%s' % (username, password)).encode('ascii')).decode()


class PigLatinContentTests(APITestCase):

    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=True,
            email='spider@pigworld.com'
        )
        self.user.set_password('acorns')
        self.user.save()

        self.pig_latin_content = PigLatinContentFactory.create(
            user=self.user, original='Spider Pig! Spider Pig!',
            translated='Iderspay Igpay! Iderspay Igpay!'
        )

    def test_authorization_fail(self):
        """Test case of failure, due to authorization fail."""
        # Create content (no credentials)
        url = reverse('contents:create-pig-latin-posts')
        response = self.client.post(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.data,
            {'detail': 'Authentication credentials were not provided.'}
        )

        # Get content (no credentials)
        url = reverse('contents:retrieve-pig-latin-posts',
                      args=(self.pig_latin_content.pk,))
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.data,
            {'detail': 'Authentication credentials were not provided.'}
        )

    def test_create_pig_latin_missing_data_fail(self):
        """Test cases of failure, due to missing data on request."""
        # First login.
        url = reverse('accounts:login')
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'acorns')
        )
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(response.data['token'])
        )

        url = reverse('contents:create-pig-latin-posts')
        response = self.client.post(url, {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'content': ['This field is required.']})

    def test_create_pig_latin_success(self):
        """Test cases of success."""
        # First login.
        url = reverse('accounts:login')
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'acorns')
        )
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(response.data['token'])
        )

        url = reverse('contents:create-pig-latin-posts')

        # Words that start with a vowel (A, E, I, O, U) simply have "WAY"
        # appended to the end of the word.
        data = {
            'content': 'important'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'importantway')

        # Words that start with a consonant have all consonant letters up to
        # the first vowel moved to the end of the word (as opposed to just
        # the first consonant letter), and "AY" is appended.
        data = {
            'content': 'spider'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'iderspay')

        # Ensures proper capitalization.
        # Correct upper case and lower case formatting.
        data = {
            'content': 'Spider PIG'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'Iderspay IGPAY')

        # Correctly translates "qu" (e.g., ietquay instead of uietqay)
        data = {
            'content': 'quiet'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'ietquay')

        # Differentiates between "Y" as vowel and "Y" as consonant.
        data = {
            'content': 'yellow style'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'ellowyway ylestay')

        # Hyphenated words are treated as two words.
        data = {
            'content': 'swing-web'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], 'ingsway-ebway')

        # Words may consist of alphabetic characters only (A-Z and a-z).
        data = {
            'content': 'food4pigs'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['translated'], '')

        # Full sentence.
        data = {
            'content': 'Spider Pig! Spider Pig! Does whatever a spider pig does!'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data['translated'],
            'Iderspay Igpay! Iderspay Igpay! Oesday ateverwhay away iderspay igpay oesday!'
        )

    def test_retrieve_pig_latin_fail(self):
        """Test cases of failure."""
        # First login.
        url = reverse('accounts:login')
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'acorns')
        )
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(response.data['token'])
        )

    def test_retrieve_pig_latin_success(self):
        """Test cases of success."""
        url = reverse('accounts:login')
        self.client.credentials(
            HTTP_AUTHORIZATION=get_basic_auth_header('spider@pigworld.com', 'acorns')
        )
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(response.data['token'])
        )

        url = reverse('contents:retrieve-pig-latin-posts',
                      args=(self.pig_latin_content.pk,))
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['content_id'], self.pig_latin_content.pk)
