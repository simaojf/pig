import factory

from django.test import TestCase

from tests.accounts.test_models import UserFactory
from contents.models import PigLatinContent


class PigLatinContentFactory(factory.DjangoModelFactory):

    class Meta:
        model = PigLatinContent


class PigLatinContentModelsTests(TestCase):
    def setUp(self):
        self.user = UserFactory.create(
            first_name='Spider', last_name='Pig', is_active=False,
            email='spider@pigworld.com'
        )
        self.pig_latin_content = PigLatinContentFactory.create(
            user=self.user, original='', translated=''
        )

    def test_string(self):
        """Check if str is correct."""
        self.assertEqual(
            str(self.pig_latin_content),
            '%s %s' % (self.user, self.pig_latin_content.timestamp)
        )
