from django.db import models


class PigLatinContent(models.Model):

    user = models.ForeignKey('accounts.User')
    original = models.TextField()
    translated = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """
        String representation for a Pig Latin translation.

        :return: string
        """
        return '%s %s' % (self.user, self.timestamp)
