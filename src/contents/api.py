from django.core.urlresolvers import reverse
from django.db import transaction
from knox.auth import TokenAuthentication
from rest_framework import status
from rest_framework.generics import get_object_or_404, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from contents.models import PigLatinContent
from contents.serializers import ContentSerializer
from lib.utils import to_pig_latin


class CreatePigLatinContentAPI(GenericAPIView):
    serializer_class = ContentSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    @transaction.atomic  # allow database rollback in case of error
    def post(self, request):
        """Post a new content."""

        # Validate data.
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Translate content.
        translated_content = to_pig_latin(serializer.validated_data['content'])

        pig_latin_content = PigLatinContent.objects.create(
            user=request.user, original=serializer.validated_data['content'],
            translated=translated_content
        )

        data = {
            'content_id': pig_latin_content.id,
            'translated': translated_content,
            'link': reverse('contents:retrieve-pig-latin-posts',
                            args=(pig_latin_content.id,))
        }
        return Response(data=data, status=status.HTTP_201_CREATED)


class RetrievePigLatinContentAPI(GenericAPIView):
    serializer_class = ()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, content_id):
        """Get a PigLatinContent by it's id."""

        # Search for a Pig Latin translation.
        pig_latin_content = get_object_or_404(
            PigLatinContent, user=request.user, id=content_id
        )

        data = {
            'content_id': pig_latin_content.id,
            'original': pig_latin_content.original,
            'translated': pig_latin_content.translated
        }
        return Response(data=data, status=status.HTTP_200_OK)
