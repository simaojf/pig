from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import contents.api

urlpatterns = [
    url(_(r'^$'), contents.api.CreatePigLatinContentAPI.as_view(),
        name='create-pig-latin-posts'),
    url(_(r'^(?P<content_id>[0-9]+)/$'),
        contents.api.RetrievePigLatinContentAPI.as_view(),
        name='retrieve-pig-latin-posts'),
]
