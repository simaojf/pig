from rest_framework import serializers


class ContentSerializer(serializers.Serializer):
    """Serializer for pig latin content ."""

    content = serializers.CharField()
