from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _

import accounts.api

urlpatterns = [

    url(_(r'^register/$'), accounts.api.UserRegisterAPI.as_view(),
        name='register'),
    url(_(r'^login/$'),
        accounts.api.UserLoginAPI.as_view(),
        name='login'),
    url(_(r'^confirm/(?P<activation_key>[0-9A-Fa-f\-]+)/$'),
        accounts.api.UserConfirmAPI.as_view(),
        name='email-confirmation'),
]
