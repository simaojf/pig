from rest_framework import serializers

from accounts.models import User
from django.utils.translation import ugettext_lazy as _

from lib.utils import full_validate_email as email_is_valid


class UserRegistrationSerializer(serializers.ModelSerializer):
    """Serializer for user registration."""

    password = serializers.CharField(max_length=128, min_length=4)
    confirm_password = serializers.CharField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password',
                  'confirm_password')

    @classmethod
    def create(cls, validated_data):
        """
        Register a user.

        :param validated_data: serializer cleaned data
        :return: User
        """
        user = User.objects.create_user(
            email=validated_data['email'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        return user

    @classmethod
    def validate(cls, data):
        """
        Confirm if passwords are the same.

        :param data: serializer data
        :return: serializer attributes
        """
        password = data.get('password', None)
        confirm_password = data.get('confirm_password', None)

        if password and confirm_password and (password != confirm_password):
            raise serializers.ValidationError(_('The passwords don\'t match.'))

        return data

    @classmethod
    def validate_email(cls, value):
        """
        Validate if email is valid.

        :param value: email string
        :return: email string
        """
        if not email_is_valid(value):
            raise serializers.ValidationError(_('Invalid email address.'))

        return value
