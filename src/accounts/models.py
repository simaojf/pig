import uuid

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def _create_user(self, email, password, first_name, last_name,
                     **extra_fields):
        """
        Create and save an User.

        :param email: string
        :param password: string
        :param first_name: string
        :param last_name: string
        :param extra_fields:
        :return: User
        """
        email = self.normalize_email(email)
        user = self.model(
            email=email, first_name=first_name, last_name=last_name,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, first_name, last_name, password,
                    **extra_fields):
        """
        Create and save an User with the given email, password and name.

        :param email: string
        :param first_name: string
        :param last_name: string
        :param password: string
        :param extra_fields:
        :return: User
        """
        return self._create_user(email, password, first_name, last_name,
                                 **extra_fields)


class User(AbstractBaseUser):
    """Model representing user."""

    first_name = models.CharField(
        _('first name'), max_length=125, null=True, blank=True
    )
    last_name = models.CharField(
        _('last name'), max_length=125, null=True, blank=True
    )
    email = models.EmailField(
        verbose_name='email address', max_length=255, unique=True
    )
    is_active = models.BooleanField(_('active'), default=False)

    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    date_updated = models.DateTimeField(_('date updated'), auto_now=True)

    # The activation key should expire, but for demo purpose it will not.
    activation_key = models.UUIDField(unique=True, default=uuid.uuid4)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self):
        """
        String representation for a user.

        :return: string
        """
        return self.email

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.

        :return: string
        """
        full_name = '%s %s' % (self.first_name or '', self.last_name or '')
        return full_name.strip()

    def get_short_name(self):
        """
        Return the first_name.

        :return: string
        """
        return self.first_name
