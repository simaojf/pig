# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('first_name', models.CharField(null=True, verbose_name='first name', max_length=125, blank=True)),
                ('last_name', models.CharField(null=True, verbose_name='last name', max_length=125, blank=True)),
                ('email', models.EmailField(unique=True, verbose_name='email address', max_length=255)),
                ('is_active', models.BooleanField(default=False, verbose_name='active')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='date joined')),
                ('date_updated', models.DateTimeField(verbose_name='date updated', auto_now=True)),
                ('activation_key', models.UUIDField(unique=True, default=uuid.uuid4)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
