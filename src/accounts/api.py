from django.contrib.auth.signals import user_logged_in
from django.core.urlresolvers import reverse
from django.db import transaction
from knox.models import AuthToken
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import get_object_or_404, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import User
from accounts.serializers import UserRegistrationSerializer
from lib.tasks import send_welcome_email
from lib.utils import get_full_current_site


class UserRegisterAPI(GenericAPIView):
    serializer_class = UserRegistrationSerializer
    permission_classes = ()
    authentication_classes = ()

    @transaction.atomic  # allow database rollback in case of error
    def post(self, request):
        """User registration view."""

        # Validate data.
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Create user.
        user = serializer.create(serializer.validated_data)

        # Send activation email to the user.
        confirmation_url = '{}{}'.format(
            get_full_current_site(request),
            reverse('accounts:email-confirmation', args=(user.activation_key,))
        )
        send_welcome_email(receiver_email=user.email, url=confirmation_url)

        return Response(status=status.HTTP_201_CREATED)


class UserLoginAPI(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        """User login with username and password."""
        token = AuthToken.objects.create(request.user)
        user_logged_in.send(sender=request.user.__class__, request=request,
                            user=request.user)
        return Response({'token': token}, status=status.HTTP_200_OK)


class UserConfirmAPI(GenericAPIView):
    permission_classes = ()
    authentication_classes = ()

    def get(self, request, activation_key):
        """Confirm user email and activate user."""

        # Search for a user with the same activation key.
        user = get_object_or_404(User, activation_key=activation_key)

        if not user.is_active:
            user.is_active = True
            user.save()

        return Response(status=status.HTTP_200_OK)
