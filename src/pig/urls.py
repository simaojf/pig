from django.conf.urls import include, url
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


urlpatterns = [
    url(_(r'^accounts/'), include('accounts.urls', namespace='accounts')),
    url(_(r'^posts/'), include('contents.urls', namespace='contents')),
]

# On dev, display a interactive API documentation.
# This is provided by django rest framework >= 3.6.
if settings.DEBUG:
    # from rest_framework.documentation import include_docs_urls
    #
    # API_TITLE = 'PIG LATIN API'
    # API_DESCRIPTION = 'API for pig latin translation support.'
    # urlpatterns += [
    #     url(r'^docs/', include_docs_urls(title=API_TITLE,
    #                                      description=API_DESCRIPTION))
    # ]
    urlpatterns += [
        url(r'^docs/', include('rest_framework_docs.urls'))
    ]
