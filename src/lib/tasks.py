from django.conf import settings

from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.core.mail import send_mail


def send_welcome_email(receiver_email, url):
    """
    Send welcome email.
    
    NOTE: this should be a celery task or something similar.
    :param receiver_email: user email
    :param url: confirmation url
    """
    send_mail(
        _('Confirm registration'),
        mark_safe('{0} <a href="{1}">{1}</a>'.format(
            _('Hello, to confirm your registration, please click on the link:'),
            url
        )),
        settings.EMAIL_NO_REPLY,
        [receiver_email],
        fail_silently=False,
    )
