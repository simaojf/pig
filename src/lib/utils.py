import re
from disposable_email_checker.validators import validate_disposable_email

from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError


def full_validate_email(value):
    """
    Validate a single email.
    
    :param value: email
    :return True for valid, False for invalid 
    """
    # Check with the disposable list.
    try:
        validate_disposable_email(value)
    except ValidationError:
        return False
    else:
        return True


def get_full_current_site(request):
    """An extension of django get_current_site(), to also return the protocol."""
    return '{protocol}://{domain}'.format(
        protocol='https' if request.is_secure() else 'http',
        domain=get_current_site(request))


def to_pig_latin(content):
    """
    Translate a string to pig latin.
    
    :param content: string to translate
    :return: string with translation
    """

    # Regex to match the different parts of the string:
    # ([qu]*|[^aeiouy]*) from consonant or the especial 'qu' to vowel
    # ([aeiouy][a-z']*) from vowel to a consonant
    # (\S?)") punctuation
    regex = re.compile(r"([qu]*|[^aeiouy]*)([aeiouy][a-z']*)(\S?)")

    result = []
    # For ech word, parse the word on the regex.
    for word in content.split():

        # Hyphenated words are treated as two words.
        if re.search(r'-', word):
            result.append(
                to_pig_latin(' '.join(word.split('-'))).replace(' ', '-')
            )
            continue

        # Ensures the word don't have numbers.
        if re.search(r'\d', word):
            continue

        word_parts = (regex.match(word.lower()).groups())
        if word[0].lower() == 'y':  # special case
            new_word = '{1}yway{2}'.format(*word_parts)[1:]
        elif word_parts[0] == '':  # word stats with a vowel
            new_word = '{1}way{2}'.format(*word_parts)
        else:
            new_word = '{1}{0}ay{2}'.format(*word_parts)

        if word.istitle():  # the first original letter is capitalized
            result.append(new_word.title())
        elif word.isupper():
            result.append(new_word.upper())
        else:
            result.append(new_word)

    return ' '.join(result)
