# Pig Latin

Pig Latin translator

## Readme Notes

* Command line starts with $, the command should run with user privileges
* Command line starts with #, the command should run with root privileges

## Retrieve code

* `$ git clone https://simaojf@bitbucket.org/simaojf/pig.git`

## Installation

* `# apt-get install build-essential libssl-dev libffi-dev python3-dev python-dev`

* `$ virtualenv -p /usr/bin/python3 pig`
* `$ cd pig`
* `$ source bin/activate`
* `$ pip install -r requirements/dev.txt`
* `$ cd src`
* `$ python manage.py migrate`

## Running

Run Django development http server

* `$ python manage.py runserver`

## Testing

Backend (django/python tests)

* `$ ./scripts/test_local_backend.sh`

## Static analysis (code style and security)

* `$ ./scripts/static_validate_backend.sh`


## NOTES

* The backend is developed in django.
* Database in sqlite3.
* When a user is registered, the confirmation code will be shown on the console. It's a simple text email.
* For coverage results, after running the test_local_backend script, open index.html from htmlcov folder.
* To test manually, go to: http://localhost:8000/docs/
